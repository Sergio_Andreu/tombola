<?php

declare(strict_types=1);

namespace Views;

class ParticipanteForm extends View
{
    private \Models\ParticipanteForm $modelo;

    public function __construct(\Models\ParticipanteForm $modelo)
    {
        $this->modelo = $modelo;
    }

    public function render(): string
    {
        if ($this->modelo->getAccion()) {

            $msj = 'Inscripción realizada';
            header("Location: " . URL_BASE . "participante/?e=0&m=$msj");
            exit;
        } elseif (!empty($this->modelo->getErrores())) {

            $msj = implode('<br>', $this->modelo->getErrores());
            header("Location: " . URL_BASE . "participante/?e=1&m=$msj");
            exit;
        } else {

            $titulo = 'Inscripciones';
            ob_start();
?>
            <div class="container px-4 px-lg-5">
                <div class="row my-5">
                    <div class="col mb-5">
                        <div class="card h-100">
                            <form class="" action="<?= URL_BASE ?>participante/inscribir/" method="POST">

                                <div class="card-body">
                                    <h2 class="card-title">Inscribir participante</h2>
                                    <div class="row mb-3">
                                        <label for="input_nombre" class="form-label">Nombre</label>
                                        <input type="text" name="nombre" class="form-control" id="input_nombre" required minlength="<?= MIN_NAME_LENGTH ?>" maxlength="<?= MAX_NAME_LENGTH ?>" placeholder="Nombre del participante">
                                    </div>
                                    <div class="row mb-3">
                                        <label for="input_apellido1" class="form-label">Apellido 1</label>
                                        <input type="text" name="apellido1" class="form-control" id="input_apellido1" required minlength="<?= MIN_NAME_LENGTH ?>" maxlength="<?= MAX_NAME_LENGTH ?>" placeholder="Apellido 1 del participante">
                                    </div>
                                    <div class="row mb-3">
                                        <label for="input_apellido2" class="form-label">Apellido 2</label>
                                        <input type="text" name="apellido2" class="form-control" id="input_apellido2" required minlength="<?= MIN_NAME_LENGTH ?>" maxlength="<?= MAX_NAME_LENGTH ?>" placeholder="Apellido 2 del participante">
                                    </div>
                                </div>

                                <div class="card-footer"><button type="submit" class="btn btn-primary">Inscribir</button></div>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
<?php
            $contenido = ob_get_clean();

            ob_start();
            $this->plantilla($titulo, $contenido);
            $html = ob_get_clean();

            return $html;
        }
    }
}
