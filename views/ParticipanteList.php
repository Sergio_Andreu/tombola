<?php

declare(strict_types=1);

namespace Views;

class ParticipanteList extends View
{
    private \Models\ParticipanteList $modelo;

    public function __construct(\Models\ParticipanteList $modelo)
    {
        $this->modelo = $modelo;
    }

    public function render(): string
    {
        $titulo = 'Lista participantes';
        ob_start();
?>
        <div class="container px-4 px-lg-5">
            <div class="row my-5">
                <div class="col mb-5">
                    <div class="card h-100">

                        <div class="card-body">
                            <h2 class="card-title">Participantes del sorteo</h2>
                            <?php if ($this->modelo->getNumeroParticipantes() == 0) : ?>
                                <p class="card-text">No hay participantes inscritos</p>
                                <a href="<?= URL_BASE ?>participante/" class="btn btn-primary">Ir a inscripciones</a>
                        </div>
                    <?php else : ?>
                        <ol start="<?php echo $this->modelo->getInicio() + 1; ?>">
                            <?php foreach ($this->modelo->getLista() as $participante) : ?>
                                <li>
                                    <?php echo $participante['nombre'] . ' ' . $participante['apellido1'] . ' ' . $participante['apellido2']; ?>
                                </li>
                            <?php endforeach; ?>
                        </ol>
                    </div>

                    <div class="card-footer">
                        <ul class="pagination">
                            <li class="page-item<?php echo ($this->modelo->getPagina() == 1) ? ' disabled' : '' ?>">
                                <a class="page-link" href="?pagina=<?php echo $this->modelo->getPagina() - 1 ?>">
                                    &laquo;
                                </a>
                            </li>

                            <?php for ($i = 1; $i <= $this->modelo->getNumeroPaginas(); $i++) : ?>
                                <li class="page-item<?php echo ($this->modelo->getPagina() == $i) ? ' active' : '' ?>">
                                    <a class='page-link' href='?pagina=<?= $i ?>'>
                                        <?= $i ?>
                                    </a>
                                </li>
                            <?php endfor; ?>

                            <li class="page-item<?php echo ($this->modelo->getPagina() == $this->modelo->getNumeroPaginas()) ? ' disabled' : '' ?>">
                                <a class="page-link" href="?pagina=<?php echo $this->modelo->getPagina() + 1 ?>">
                                    &raquo;
                                </a>
                            </li>
                        </ul>
                    </div>
                <?php endif; ?>
                </div>
            </div>
        </div>
        </div>

<?php
        $contenido = ob_get_clean();

        ob_start();
        $this->plantilla($titulo, $contenido);
        $html = ob_get_clean();

        return $html;
    }
}
