<?php

declare(strict_types=1);

namespace Views;

abstract class View
{
    abstract public function render();

    protected function plantilla(string $titulo, string $contenido): void
    {

        $this->incHeader($titulo);

        if (isset($_GET['m']) && $_GET['m'] != '') {
            $this->incModalNotificacion(isset($_GET['e']) && $_GET['e'] == 1 ? 'Error' : 'Notificación', $_GET['m']);
        }

        $this->incNav();

        echo $contenido;

        $this->incFooter();
    }


    protected function incHeader(string $titulo): void
    {
?>
        <!DOCTYPE html>
        <html lang="es">

        <head>
            <meta charset="UTF-8">
            <meta http-equiv="X-UA-Compatible" content="IE=edge">
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
            <title><?= $titulo ?> - Tómbola Montessori Ecijana</title>
            <!-- Favicon-->
            <link rel="icon" type="image/x-icon" href="<?= URL_BASE ?>assets/favicon.ico" />
            <!-- Estilos -->
            <link rel="stylesheet" href="<?= URL_BASE ?>css/styles.css">
            <!-- <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.6.1/font/bootstrap-icons.css"> -->

        </head>

        <body>
        <?php
    }

    protected function incModalNotificacion(string $titulo, string $contenido): void
    {
        ?>
            <div class="modal fade" id="modal_notificacion" tabindex="-1">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title"><?= $titulo ?></h5>
                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>
                        <div class="modal-body">
                            <?= $contenido ?>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-primary" data-bs-dismiss="modal">Aceptar</button>
                        </div>
                    </div>
                </div>
            </div>
        <?php
    }

    protected function incNav(): void
    {
        ?>
            <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
                <div class="container px-5">
                    <a class="navbar-brand" href="<?= URL_BASE ?>">TME</a>
                    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="collapse navbar-collapse" id="navbar_nav">
                        <ul class="navbar-nav ms-auto mb-2 mb-lg-0">
                            <li class="nav-item">
                                <a class="nav-link" href="<?= URL_BASE ?>">Sorteo</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="<?= URL_BASE ?>participante/">Inscribir participante</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="<?= URL_BASE ?>lista/">Lista participantes</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>
        <?php
    }

    protected function incFooter(): void
    {
        ?>
            <footer class="py-5 bg-dark">
                <div class="container px-4 px-lg-5">
                    <p class="m-0 text-center text-white">TME - <?php echo date('Y'); ?></p>
                </div>
            </footer>
            <script src="<?= URL_BASE ?>js/bootstrap.bundle.js"></script>
            <script src="<?= URL_BASE ?>js/scripts.js"></script>
        </body>

        </html>
<?php
    }
}
