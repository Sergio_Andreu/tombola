<?php

declare(strict_types=1);

namespace Views;

class Sorteo extends View
{
    private \Models\Sorteo $modelo;

    public function __construct(\Models\Sorteo $modelo)
    {
        $this->modelo = $modelo;
    }

    public function render(): string
    {
        if ($this->modelo->getAccion()) {

            header("Location: " . URL_BASE);
            exit;
        } elseif (!empty($this->modelo->getErrores())) {

            $msj = implode('<br>', $this->modelo->getErrores());
            header("Location: " . URL_BASE . "?e=1&m=$msj");
            exit;
        } else {

            $titulo = 'Sorteo';
            ob_start();
?>
            <div class="container px-4 px-lg-5">
                <div class="row align-items-center my-5">
                    <div class="col">
                        <h1 class="font-weight-light">Tómbola Montessori Ecijana</h1>
                    </div>
                </div>
                <div class="row">
                    <div class="col mb-5">
                        <div class="card h-100">

                            <div class="card-body">
                                <?php if ($this->modelo->getNumeroParticipantes() == 0) : ?>
                                    <p class="card-text">No hay participantes inscritos</p>
                            </div>

                            <div class="card-footer"><a href="<?= URL_BASE ?>participante/" class="btn btn-primary">Ir a inscripciones</a></div>

                        <?php else : ?>
                            <?php if ($this->modelo->sorteoRealizado() === true) : ?>
                                <h2 class="card-title">Resultados del sorteo</h2>
                                <ul class="list-group">
                                    <?php foreach ($this->modelo->getResultadoSorteo() as $mensaje) : ?>
                                        <li class="list-group-item"><?= $mensaje ?></li>
                                    <?php endforeach; ?>
                                </ul>
                        </div>

                        <div class="card-footer"><a href="<?= URL_BASE ?>resetear/" class="btn btn-danger">Resetear sorteo</a></div>

                    <?php else : ?>
                        <p class="card-text">Hay <?php echo $this->modelo->getNumeroParticipantes() ?> participantes inscritos</p>
                    </div>

                    <div class="card-footer"><a href="<?= URL_BASE ?>sortear/" class="btn btn-success">¡Realizar Sorteo!</a></div>

                <?php endif; ?>
            <?php endif; ?>
                </div>
            </div>
            </div>
            </div>
<?php
            $contenido = ob_get_clean();

            ob_start();
            $this->plantilla($titulo, $contenido);
            $html = ob_get_clean();

            return $html;
        }
    }
}
