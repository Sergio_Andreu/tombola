<?php

declare(strict_types=1);

namespace Core;

class Request
{
    private array $get;
    private array $post;
    private string $request_uri_limpia;
    private array $server;

    public function __construct(array $get, array $post, array $server)
    {
        $this->get = $get;
        $this->post = $post;
        $this->server = $server;
        $this->request_uri_limpia = parse_url($server['REQUEST_URI'], PHP_URL_PATH);
    }

    public function getUri(): string
    {
        return $this->request_uri_limpia;
    }

    public function getGet(): array
    {
        return $this->get;
    }

    public function getPost(): array
    {
        return $this->post;
    }

    public function getServer(): array
    {
        return $this->server;
    }
}
