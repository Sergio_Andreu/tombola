<?php

declare(strict_types=1);

namespace Models;

class ParticipanteForm
{
    private \Core\DB $db;
    private bool $accion_realizada;
    private array $errores;

    public function __construct(\Core\DB $db)
    {
        $this->db = $db;
        $this->accion_realizada = false;
        $this->errores = [];
    }

    public function inscribir(array $datos): void
    {
        $this->errores = $this->validar(
            $datos['nombre'],
            $datos['apellido1'],
            $datos['apellido2']
        );

        if (empty($this->errores)) {
            $this->insert(
                $datos['nombre'],
                $datos['apellido1'],
                $datos['apellido2']
            );
        }
    }

    private function validar(string $nombre, string $apellido1, string $apellido2): array
    {
        $errores = [];
        if (empty($nombre)) {
            $errores[] = 'El campo \'nombre\' es obligatorio';
        } elseif (strlen($nombre) < MIN_NAME_LENGTH) {
            $errores[] = 'El campo \'nombre\' debe tener una longitud mayor o igual que ' . MIN_NAME_LENGTH;
        } elseif (strlen($nombre) > MAX_NAME_LENGTH) {
            $errores[] = 'El campo \'nombre\' debe tener una longitud menor o igual que ' . MAX_NAME_LENGTH;
        }
        if (empty($apellido1)) {
            $errores[] = 'El campo \'apellido1\' es obligatorio';
        } elseif (strlen($apellido1) < MIN_NAME_LENGTH) {
            $errores[] = 'El campo \'apellido1\' debe tener una longitud mayor o igual que ' . MIN_NAME_LENGTH;
        } elseif (strlen($apellido1) > MAX_NAME_LENGTH) {
            $errores[] = 'El campo \'apellido1\' debe tener una longitud menor o igual que ' . MAX_NAME_LENGTH;
        }
        if (empty($apellido2)) {
            $errores[] = 'El campo \'apellido2\' es obligatorio';
        } elseif (strlen($apellido2) < MIN_NAME_LENGTH) {
            $errores[] = 'El campo \'apellido2\' debe tener una longitud mayor o igual que ' . MIN_NAME_LENGTH;
        } elseif (strlen($apellido2) > MAX_NAME_LENGTH) {
            $errores[] = 'El campo \'apellido2\' debe tener una longitud menor o igual que ' . MAX_NAME_LENGTH;
        }
        return $errores;
    }

    private function insert(string $nombre, string $apellido1, string $apellido2): void
    {
        $sql = "INSERT INTO rifa (nombre, apellido1, apellido2, premio) VALUES (?, ?, ?, '')";
        $parametros = [$nombre, $apellido1, $apellido2];
        if ((bool) $this->db->ejecutar($sql, $parametros)->rowCount()) {
            $this->accion_realizada = true;
        }
    }

    public function getAccion(): bool
    {
        return $this->accion_realizada;
    }

    public function getErrores(): array
    {
        return $this->errores;
    }
}
