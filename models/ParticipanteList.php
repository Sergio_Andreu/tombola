<?php

declare(strict_types=1);

namespace Models;

class ParticipanteList
{
    private \Core\DB $db;
    private int $items_pagina;
    private int $pagina;
    private int $numero_paginas;
    private int $inicio;

    public function __construct(\Core\DB $db)
    {
        $this->db = $db;
        $this->lista_paginada = [];
        $this->items_pagina = 20;
        $this->pagina = 1;
        $this->numero_paginas = (int) ceil($this->getNumeroParticipantes() / $this->items_pagina);
        $this->inicio = ($this->pagina > 1) ? ($this->pagina * $this->items_pagina - $this->items_pagina) : 0;
    }

    public function getLista(): array
    {
        $sql = 'SELECT SQL_CALC_FOUND_ROWS *
                FROM rifa
                ORDER BY id ASC
                LIMIT ?, ?';

        return $this->db->ejecutar($sql, [$this->inicio, $this->items_pagina])->fetchAll();
    }

    public function getNumeroParticipantes(): int
    {
        return (int) $this->db->ejecutar('SELECT COUNT(1) FROM rifa')->fetchColumn();
    }

    public function getPagina(): int
    {
        return $this->pagina;
    }

    public function getNumeroPaginas(): int
    {
        return $this->numero_paginas;
    }

    public function getInicio(): int
    {
        return $this->inicio;
    }

    public function setPagina(int $pagina): void
    {
        $this->pagina = $pagina;
        $this->inicio = ($this->pagina > 1) ? ($this->pagina * $this->items_pagina - $this->items_pagina) : 0;
    }
}
