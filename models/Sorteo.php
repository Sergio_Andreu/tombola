<?php

declare(strict_types=1);

namespace Models;

class Sorteo
{
    private \Core\DB $db;
    private bool $accion_realizada;
    private array $errores;
    private array $premios;


    public function __construct(\Core\DB $db)
    {
        $this->db = $db;
        $this->accion_realizada = false;
        $this->errores = [];
        $this->premios = require DIR_RAIZ . '/config/premios.php';
    }

    public function getResultadoSorteo(): array
    {
        $sql = "SELECT * FROM rifa WHERE premio <> '' ";
        $resultados = [];
        foreach ($this->db->ejecutar($sql)->fetchAll() as $resultado) {
            $nombre_completo = $resultado['nombre'] . ' ' . $resultado['apellido1'] . ' ' . $resultado['apellido2'] . ' (#' . $resultado['id'] . ')';
            $mensaje = $this->getMensajePremio($resultado['premio']);
            $resultados[] = str_replace('###', $nombre_completo, $mensaje);
        }
        return $resultados;
    }

    public function getMensajePremio(string $nombre_premio): string
    {
        $mensaje_premio = 'Premio para ###';
        foreach ($this->premios as $premio) {
            if ($premio['premio'] == $nombre_premio) {
                $mensaje_premio = $premio['mensaje'];
            }
        }
        return $mensaje_premio;
    }

    public function realizarSorteo(): void
    {
        if (!$this->sorteoRealizado()) {
            // coger id de participante aleatorio tantas veces como haya premios
            $num_participantes = $this->getNumeroParticipantes();
            $num_premios = $this->getNumeroPremios();

            // permito menos participantes que premios, en cuyo caso se quedarán premios sin entregar,
            // priorizándose los de menor id
            $num_premios_entregar = min($num_participantes, $num_premios);

            for ($i = 0; $i < $num_premios_entregar; $i++) {
                $entregado = false;

                while (!$entregado) {
                    $id_participante = rand(0, $num_participantes);
                    if (!$this->participanteTienePremio($id_participante)) {
                        $entregado = $this->entregarPremio($id_participante, $this->premios[$i]['premio']);
                    }
                }
            }

            $this->accion_realizada = true;
        } else {
            $this->errores[] = 'El sorteo ya ha tenido lugar';
        }
    }

    public function resetearSorteo(): void
    {
        $this->db->ejecutar('TRUNCATE TABLE rifa');
    }

    public function sorteoRealizado(): bool
    {
        return (bool) $this->db->ejecutar("SELECT count(1) FROM rifa WHERE premio <> '' ")->fetchColumn();
    }

    public function getNumeroParticipantes(): int
    {
        return (int) $this->db->ejecutar('SELECT COUNT(1) FROM rifa')->fetchColumn();
    }

    private function getNumeroPremios(): int
    {
        return (int) count($this->premios);
    }

    private function participanteTienePremio(int $id): bool
    {
        $sql = "SELECT COUNT(1) FROM rifa WHERE id = ? AND premio <> '' ";
        return (bool) $this->db->ejecutar($sql, [$id])->fetchColumn();
    }

    private function entregarPremio(int $id_participante, string $premio): bool
    {
        $this->db->comenzarTransacccion();
        $sql = 'UPDATE rifa SET premio = ? WHERE id = ?';
        $sentencia = $this->db->ejecutar($sql, [$premio, $id_participante]);
        $this->db->commit();
        return (bool) $sentencia->rowCount();
    }

    public function getAccion(): bool
    {
        return $this->accion_realizada;
    }

    public function getErrores(): array
    {
        return $this->errores;
    }
}
