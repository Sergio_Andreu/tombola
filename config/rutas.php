<?php

$rutas = [
    # Expresión Regular URI                Modelo                         Vista                         Controlador                         Método Cont.
    ['/^\/$/',                             '\Models\Sorteo',              '\Views\Sorteo',              null,                               null],
    ['/^\/participante\/$/',               '\Models\ParticipanteForm',    '\Views\ParticipanteForm',    null,                               null],
    ['/^\/participante\/inscribir\/$/',    '\Models\ParticipanteForm',    '\Views\ParticipanteForm',    '\Controllers\ParticipanteForm',    'inscribir'],
    ['/^\/lista\/$/',                      '\Models\ParticipanteList',    '\Views\ParticipanteList',    '\Controllers\ParticipanteList',    'lista'],
    ['/^\/sortear\/$/',                    '\Models\Sorteo',              '\Views\Sorteo',              '\Controllers\Sorteo',              'sortear'],
    ['/^\/resetear\/$/',                   '\Models\Sorteo',              '\Views\Sorteo',              '\Controllers\Sorteo',              'resetear']
];

return $rutas;
