<?php

declare(strict_types=1);
error_reporting(E_ALL);
// Mostrar errores en test
ini_set('display_errors', '1');
// Logear errores en prod, y no mostrar
// ini_set('display_errors', 0);
// ini_set('log_errors', 1);


// CONSTANTES
// Base de datos
define('DB_HOST', '127.0.0.1');
define('DB_PUERTO', '13306');
define('DB_USUARIO', 'sergio');
define('DB_PASSWORD', 'debian1121');
define('DB_NOMBRE', 'tombolaecijana');
define('DB_CHARSET', 'utf8mb4');

// Requisitos Participantes
define('MIN_NAME_LENGTH', 2);
define('MAX_NAME_LENGTH', 20);

// Constantes servidor
define('DIR_RAIZ', dirname(dirname(__FILE__))); // C:\ ... \tombola
define('URL_BASE', 'http://practica.tombola.es/');
