<?php

$premios = [
    ['premio' => 'El rico jamón',   'mensaje' => 'Y vaya Jamón se lleva ###'],
    ['premio' => 'Perrito piloto',  'mensaje' => 'Qué bonito alboroto, ### te ha tocado el perrito piloto'],
    ['premio' => 'Minimoto',        'mensaje' => 'Qué bonito, que alboroto, ### vino andando y se va en minimoto'],
    ['premio' => 'Mountain Bike',   'mensaje' => 'Guay guay guay, a ### le ha tocado una Mountain Bike'],
    ['premio' => 'Pelota',          'mensaje' => '### siempre toca, siempre toca, si no es un pito es una pelota'],
    ['premio' => 'Muñeca chochona', 'mensaje' => 'Y para ### la chochona, la muñeca chochona del verano, que bonita la chochona'],
];

return $premios;
