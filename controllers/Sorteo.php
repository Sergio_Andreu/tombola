<?php

declare(strict_types=1);

namespace Controllers;

class Sorteo
{
    private \Models\Sorteo $modelo;

    public function __construct(\Models\Sorteo $modelo)
    {
        $this->modelo = $modelo;
    }

    public function sortear(\Core\Request $request): void
    {
        $this->modelo->realizarSorteo();
    }

    public function resetear(\Core\Request $request): void
    {
        $this->modelo->resetearSorteo();
    }
}
