<?php

declare(strict_types=1);

namespace Controllers;

class ParticipanteList
{
    private \Models\ParticipanteList $modelo;

    public function __construct(\Models\ParticipanteList $modelo)
    {
        $this->modelo = $modelo;
    }

    public function lista(\Core\Request $request)
    {
        $pagina = isset($request->getGet()['pagina']) ? (int) $request->getGet()['pagina'] : 1;
        $this->modelo->setPagina($pagina);
    }
}