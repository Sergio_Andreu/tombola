<?php

declare(strict_types=1);

namespace Controllers;

class ParticipanteForm
{
    private \Models\ParticipanteForm $modelo;

    public function __construct(\Models\ParticipanteForm $modelo)
    {
        $this->modelo = $modelo;
    }

    public function inscribir(\Core\Request $request): void
    {
        $this->modelo->inscribir($request->getPost());
    }
}
